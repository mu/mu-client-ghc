--
-- Copyright 2017 The Australian National University
--
-- Licensed under the 3-Clause BSD License (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     https://opensource.org/licenses/BSD-3-Clause
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--

{-# LANGUAGE OverloadedStrings #-}

module Main where

import Data.Monoid ((<>))
import Data.Foldable (foldMap, toList)
import Data.List (isSuffixOf)
import Foreign.C.String (newCString)
import Foreign.Ptr (nullPtr)
import Foreign.Marshal.Array (newArray)
import System.Environment (getArgs)
import System.Exit (exitFailure)
import qualified Data.Sequence as Seq
import qualified Data.Map.Strict as M

import qualified Language.Haskell.GHC.Simple as GHC.Simple
import Language.Haskell.GHC.Simple.Extra ()
import Language.Haskell.GHC.Simple (CompResult (..), CompiledModule (..),
                                    CompConfig (..))

import DynFlags

import Compiler.Mu
import Mu.API
import Mu.AST
import Mu.PrettyPrint (pp)
import Mu.Impl.Fast
import Mu.Interface

import System.IO

import Debug.Trace

compilerConfig :: CompConfig
compilerConfig =
    GHC.Simple.disableCodeGen $
    GHC.Simple.defaultConfig
    { cfgUseGhcErrorLogger = True
    , cfgStopPhases = GHC.Simple.ncgPhases
    , cfgGhcFlags = ["-ddump-stg", "-ddump-cmm-raw", "-ddump-to-file"] {-"-v5",-}
    , cfgCacheDirectory = Nothing
    }

main :: IO ()
main = do
    inputFiles <- getArgs
    (dflags, _) <- GHC.Simple.getDynFlagsForConfig compilerConfig
    b <- GHC.Simple.compileWith compilerConfig (compileMu dflags) inputFiles
    libresults <- return [] --mapM compileLibraryFiles libraryFiles
    case b of
        Success results _ _ -> do
            printResults lives (mergeResults results')
            doResults lives (mergeResults results')
          where
            results' = (loadPrim dflags : concat libresults) ++ (fmap modCompiledModule results)
            lives = toList $ foldMap getDefns results'
            getDefns (defns, _, _) = fmap toName defns
        Failure _ _ -> exitFailure

compileLibraryFiles :: (String, [FilePath], [String]) -> IO [MuResult]
compileLibraryFiles (packageKey, files, extraFlags) = do
    traceM ("Compiling the library file " ++ packageKey)
    (dflags, _) <- GHC.Simple.getDynFlagsForConfig compilerConfig
    b <- GHC.Simple.compileWith libconfig (compileMu dflags) files
    case b of
        Success results _ _ -> return $ fmap modCompiledModule results
        Failure _ _ -> error "Could not compile internal bundle"
  where
    libconfig = compilerConfig
        { cfgGhcFlags =
              [ "-this-unit-id", packageKey, "-ddump-stg", "-ddump-to-file"
              , "-I/home/pavel/Development/mu/mu-client-ghc/libraries/base/include/"
              , "-I/Users/nathan/projects/microvm/anuhc/libraries/include/"]
              ++ extraFlags
        }

doResults :: [Name] -> MuMergedResult -> IO ()
doResults liveObjectNames (defns, topClosures, mainFunction) = do
    mu <- newMu
    ctx <- newContext mu
    buildBundle ctx (Bundle defns')

    mapM_ (populate ctx) topClosures

    liveObjects <- mapM (getID ctx) liveObjectNames
    whitelist <- newArray liveObjects
    let whitelistSz = fromIntegral . length $ liveObjects

    mainFunctionRef <- (case mainFunction of
        Just func -> handleFromFunc ctx =<< getID ctx func
        Nothing   -> return nullPtr)
    newCString "output.mu" >>= makeBootImage ctx
                  whitelist whitelistSz                -- object whitelist
                  mainFunctionRef nullPtr nullPtr      -- main function
                  nullPtr nullPtr 0                    -- symbols
                  nullPtr nullPtr 0                    -- relocatables
  where
    defns' = M.elems defns

-- | A list of all library files, indexed by their package-key.
-- TODO: fix this.
libraryFiles :: [(String, [FilePath], [String])]
libraryFiles = [ ("ghc-prim", prefix ghcPrimFiles, [])
               , ("base", prefix baseFiles, ["-I/home/pavel/Development/mu/mu-client-ghc/libraries/base/include/", "-i/home/pavel/Development/mu/mu-client-ghc/libraries/base"])
               , ("base", prefix baseFiles, ["-I/Users/nathan/projects/microvm/anuhc/libraries/base/include/", "-i/Users/nathan/projects/microvm/anuhc/libraries/base"])
               , ("base", prefix' baseFiles, ["-I/home/pavel/Development/mu/mu-client-ghc/libraries/base/include/", "-i/home/pavel/Development/mu/mu-client-ghc/libraries/base"])
               , ("base", prefix' baseFiles, ["-I/Users/nathan/projects/microvm/anuhc/libraries/base/include/", "-i/Users/nathan/projects/microvm/anuhc/libraries/base"])
               ]
  where
    prefix = fmap ("/home/pavel/Development/mu/mu-client-ghc/libraries/" ++)
    prefix' = fmap ("/Users/nathan/projects/microvm/anuhc/libraries/" ++)
    ghcPrimFiles =
        [ "ghc-prim/GHC/CString.hs"
        , "ghc-prim/GHC/Classes.hs"
        , "ghc-prim/GHC/Debug.hs"
        , "ghc-prim/GHC/IntWord64.hs"
        , "ghc-prim/GHC/Magic.hs"
        -- , "ghc-prim/GHC/PrimopWrappers.hs"
        , "ghc-prim/GHC/Tuple.hs"
        , "ghc-prim/GHC/Types.hs"
        ]
    baseFiles =
        [ "base/Control/Applicative.hs"
        , "base/Control/Arrow.hs"
        , "base/Control/Category.hs"
        , "base/Control/Concurrent.hs"
        , "base/Control/Concurrent/Chan.hs"
        , "base/Control/Concurrent/MVar.hs"
        , "base/Control/Concurrent/QSem.hs"
        , "base/Control/Concurrent/QSemN.hs"
        , "base/Control/Exception.hs"
        , "base/Control/Exception/Base.hs"
        , "base/Control/Monad.hs"
        , "base/Control/Monad/Fail.hs"
        , "base/Control/Monad/Fix.hs"
        , "base/Control/Monad/Instances.hs"
        , "base/Control/Monad/IO/Class.hs"
        , "base/Control/Monad/ST.hs"
        , "base/Control/Monad/ST/Lazy.hs"
        , "base/Control/Monad/ST/Lazy/Safe.hs"
        , "base/Control/Monad/ST/Lazy/Unsafe.hs"
        , "base/Control/Monad/ST/Safe.hs"
        , "base/Control/Monad/ST/Strict.hs"
        , "base/Control/Monad/ST/Unsafe.hs"
        , "base/Control/Monad/Zip.hs"
        , "base/Data/Bifunctor.hs"
        , "base/Data/Bits.hs"
        , "base/Data/Bool.hs"
        , "base/Data/Char.hs"
        , "base/Data/Coerce.hs"
        , "base/Data/Complex.hs"
        , "base/Data/Data.hs"
        , "base/Data/Dynamic.hs"
        , "base/Data/Either.hs"
        , "base/Data/Eq.hs"
        , "base/Data/Fixed.hs"
        , "base/Data/Foldable.hs"
        , "base/Data/Function.hs"
        , "base/Data/Functor.hs"
        , "base/Data/Functor/Classes.hs"
        , "base/Data/Functor/Compose.hs"
        , "base/Data/Functor/Const.hs"
        , "base/Data/Functor/Identity.hs"
        , "base/Data/Functor/Product.hs"
        , "base/Data/Functor/Sum.hs"
        , "base/Data/IORef.hs"
        , "base/Data/Int.hs"
        , "base/Data/Ix.hs"
        , "base/Data/Kind.hs"
        , "base/Data/List.hs"
        , "base/Data/List/NonEmpty.hs"
        , "base/Data/Maybe.hs"
        , "base/Data/Monoid.hs"
        , "base/Data/Ord.hs"
        , "base/Data/Proxy.hs"
        , "base/Data/Ratio.hs"
        , "base/Data/Semigroup.hs"
        , "base/Data/STRef.hs"
        , "base/Data/STRef/Lazy.hs"
        , "base/Data/STRef/Strict.hs"
        , "base/Data/String.hs"
        , "base/Data/Traversable.hs"
        , "base/Data/Tuple.hs"
        , "base/Data/Type/Bool.hs"
        , "base/Data/Type/Coercion.hs"
        , "base/Data/Type/Equality.hs"
        , "base/Data/Typeable.hs"
        , "base/Data/Typeable/Internal.hs"
        , "base/Data/Unique.hs"
        , "base/Data/Version.hs"
        , "base/Data/Void.hs"
        , "base/Data/Word.hs"
        , "base/Debug/Trace.hs"
        , "base/Foreign.hs"
        , "base/Foreign/C.hs"
        , "base/Foreign/C/Error.hs"
        , "base/Foreign/C/String.hs"
        , "base/Foreign/C/Types.hs"
        , "base/Foreign/Concurrent.hs"
        , "base/Foreign/ForeignPtr.hs"
        , "base/Foreign/ForeignPtr/Safe.hs"
        , "base/Foreign/ForeignPtr/Unsafe.hs"
        , "base/Foreign/Marshal.hs"
        , "base/Foreign/Marshal/Alloc.hs"
        , "base/Foreign/Marshal/Array.hs"
        , "base/Foreign/Marshal/Error.hs"
        , "base/Foreign/Marshal/Pool.hs"
        , "base/Foreign/Marshal/Safe.hs"
        , "base/Foreign/Marshal/Unsafe.hs"
        , "base/Foreign/Marshal/Utils.hs"
        , "base/Foreign/Ptr.hs"
        , "base/Foreign/Safe.hs"
        , "base/Foreign/StablePtr.hs"
        , "base/Foreign/Storable.hs"
        , "base/GHC/Arr.hs"
        , "base/GHC/Base.hs"
        , "base/GHC/Char.hs"
        , "base/GHC/Conc.hs"
        , "base/GHC/Conc/IO.hs"
        , "base/GHC/Conc/Signal.hs"
        , "base/GHC/Conc/Sync.hs"
        , "base/GHC/ConsoleHandler.hs"
        , "base/GHC/Constants.hs"
        , "base/GHC/Desugar.hs"
        , "base/GHC/Enum.hs"
        , "base/GHC/Environment.hs"
        , "base/GHC/Err.hs"
        , "base/GHC/Exception.hs"
        , "base/GHC/ExecutionStack.hs"
        , "base/GHC/ExecutionStack/Internal.hs"
        , "base/GHC/Exts.hs"
        , "base/GHC/Fingerprint.hs"
        , "base/GHC/Fingerprint/Type.hs"
        , "base/GHC/Float.hs"
        , "base/GHC/Float/ConversionUtils.hs"
        , "base/GHC/Float/RealFracMethods.hs"
        , "base/GHC/Foreign.hs"
        , "base/GHC/ForeignPtr.hs"
        , "base/GHC/GHCi.hs"
        , "base/GHC/Generics.hs"
        , "base/GHC/IO.hs"
        , "base/GHC/IO/Buffer.hs"
        , "base/GHC/IO/BufferedIO.hs"
        , "base/GHC/IO/Device.hs"
        , "base/GHC/IO/Encoding.hs"
        , "base/GHC/IO/Encoding/CodePage.hs"
        , "base/GHC/IO/Encoding/Failure.hs"
        , "base/GHC/IO/Encoding/Iconv.hs"
        , "base/GHC/IO/Encoding/Latin1.hs"
        , "base/GHC/IO/Encoding/Types.hs"
        , "base/GHC/IO/Encoding/UTF16.hs"
        , "base/GHC/IO/Encoding/UTF32.hs"
        , "base/GHC/IO/Encoding/UTF8.hs"
        , "base/GHC/IO/Exception.hs"
        , "base/GHC/IO/FD.hs"
        , "base/GHC/IO/Handle.hs"
        , "base/GHC/IO/Handle/FD.hs"
        , "base/GHC/IO/Handle/Internals.hs"
        , "base/GHC/IO/Handle/Text.hs"
        , "base/GHC/IO/Handle/Types.hs"
        , "base/GHC/IO/IOMode.hs"
        , "base/GHC/IO/Unsafe.hs"
        , "base/GHC/IOArray.hs"
        , "base/GHC/IORef.hs"
        , "base/GHC/Int.hs"
        , "base/GHC/List.hs"
        , "base/GHC/MVar.hs"
        , "base/GHC/Natural.hs"
        , "base/GHC/Num.hs"
        , "base/GHC/OldList.hs"
        , "base/GHC/OverloadedLabels.hs"
        , "base/GHC/PArr.hs"
        , "base/GHC/Pack.hs"
        , "base/GHC/Profiling.hs"
        , "base/GHC/Ptr.hs"
        , "base/GHC/Read.hs"
        , "base/GHC/Real.hs"
        , "base/GHC/RTS/Flags.hs"
        , "base/GHC/ST.hs"
        , "base/GHC/StaticPtr.hs"
        , "base/GHC/STRef.hs"
        , "base/GHC/Show.hs"
        , "base/GHC/Stable.hs"
        , "base/GHC/Stack.hs"
        , "base/GHC/Stack/CCS.hs"
        , "base/GHC/Stack/Types.hs"
        , "base/GHC/Stats.hs"
        , "base/GHC/Storable.hs"
        , "base/GHC/TopHandler.hs"
        , "base/GHC/TypeLits.hs"
        , "base/GHC/Unicode.hs"
        , "base/GHC/Weak.hs"
        , "base/GHC/Word.hs"
        , "base/Numeric.hs"
        , "base/Numeric/Natural.hs"
        , "base/Prelude.hs"
        , "base/System/CPUTime.hs"
        , "base/System/Console/GetOpt.hs"
        , "base/System/Environment.hs"
        , "base/System/Exit.hs"
        , "base/System/IO.hs"
        , "base/System/IO/Error.hs"
        , "base/System/IO/Unsafe.hs"
        , "base/System/Info.hs"
        , "base/System/Mem.hs"
        , "base/System/Mem/StableName.hs"
        , "base/System/Mem/Weak.hs"
        , "base/System/Posix/Internals.hs"
        , "base/System/Posix/Types.hs"
        , "base/System/Timeout.hs"
        , "base/Text/ParserCombinators/ReadP.hs"
        , "base/Text/ParserCombinators/ReadPrec.hs"
        , "base/Text/Printf.hs"
        , "base/Text/Read.hs"
        , "base/Text/Read/Lex.hs"
        , "base/Text/Show.hs"
        , "base/Text/Show/Functions.hs"
        , "base/Unsafe/Coerce.hs"
        ]

printResults :: [Name] -> MuMergedResult -> IO ()
printResults liveObjectNames (defns, topClosures, mainFunction) = do
    putStrLn "\n********** COMPILATION FINISHED **********\n"
    putStrLn $ "Live object names: "
    mapM_ print liveObjectNames
    putStrLn ""
    
    putStrLn "Definitions:"
    let strDefns = M.mapWithKey (\name -> \defn -> pp defn) defns
    mapM_ putStrLn strDefns
    putStrLn ""
    
    putStrLn "Top-level closures:"
    let strClosures = M.mapWithKey (\name -> \closure -> show closure) topClosures
    mapM_ putStrLn strClosures
    putStrLn ""
    
    case mainFunction of
        Just name -> putStrLn $ "Primordial function: " ++ (pp name)
        Nothing -> return ()
    
    putStrLn "\n********** LOADING BUNDLE **********\n"
    hFlush stdout
    hFlush stderr
    return ()
        
