{-# LANGUAGE FlexibleInstances, CPP #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
-- | Lower level building blocks for custom code generation.
module Language.Haskell.GHC.Simple.Extra (
    toSimplifiedStgPlus
  ) where

-- GHC scaffolding
import GHC hiding (Warning)
import GhcMonad (liftIO)
import HscTypes
import CostCentre
import CorePrep
import StgSyn
import CoreSyn
import CoreToStg
import SimplStg
import DriverPipeline

import Language.Haskell.GHC.Simple.Types

instance Intermediate ([TyCon], [StgBinding]) where
  prepare = toSimplifiedStgAndTyCons

instance Intermediate ([TyCon], [StgBinding], CollectedCCs) where
  prepare = toSimplifiedStgPlus

toSimplifiedStgAndTyCons :: ModSummary -> CgGuts -> CompPipeline ([TyCon], [StgBinding])
toSimplifiedStgAndTyCons ms cgguts = do
  env <- hsc_env `fmap` getPipeState
  let dfs = hsc_dflags env
  liftIO $ do
    prog <- prepareCore env dfs ms cgguts
    stg <- coreToStg dfs (ms_mod ms) prog
    (binds, _) <- stg2stg dfs (ms_mod ms) stg
    return (cg_tycons cgguts, binds)

-- | Compile a 'ModSummary' into a list of simplified 'StgBinding's.
--   See <https://ghc.haskell.org/trac/ghc/wiki/Commentary/Compiler/StgSynType>
--   for more information about STG and how it relates to core and Haskell.
toSimplifiedStgPlus :: ModSummary -> CgGuts -> CompPipeline ([TyCon], [StgBinding], CollectedCCs)
toSimplifiedStgPlus ms cgguts = do
  env <- hsc_env `fmap` getPipeState
  let dfs = hsc_dflags env
  liftIO $ do
    prog <- prepareCore env dfs ms cgguts
    stg <- coreToStg dfs (ms_mod ms) prog
    (binds, ccs) <- stg2stg dfs (ms_mod ms) stg
    return (cg_tycons cgguts, binds, ccs)

-- | Prepare a core module for code generation.
prepareCore :: HscEnv -> DynFlags -> ModSummary -> CgGuts -> IO CoreProgram
prepareCore env dfs _ms p = do
#if __GLASGOW_HASKELL__ >= 800
  liftIO $ corePrepPgm env (ms_mod _ms) (ms_location _ms) (cg_binds p) (cg_tycons p)
#elif __GLASGOW_HASKELL__ >= 710
  liftIO $ corePrepPgm env (ms_location _ms) (cg_binds p) (cg_tycons p)
#else
  liftIO $ corePrepPgm dfs env (cg_binds p) (cg_tycons p)
#endif
