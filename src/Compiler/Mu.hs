--
-- Copyright 2017 The Australian National University
--
-- Licensed under the 3-Clause BSD License (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     https://opensource.org/licenses/BSD-3-Clause
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--

module Compiler.Mu
    ( compileMu
    , MuResult
    , MuMergedResult
    , mergeResults
    , populate
    , loadPrim
    ) where

import TyCon (TyCon)
import StgSyn (StgBinding)
import Language.Haskell.GHC.Simple (ModMetadata (..))

import DynFlags

import Compiler.Mu.FromSTG (stgToMu)
import Compiler.Mu.CodeGen (MuResult, MuMergedResult, mergeResults)
import Compiler.Mu.PrimOps (loadPrim)
import Compiler.Mu.VM      (populate)

compileMu :: DynFlags -> ModMetadata -> ([TyCon], [StgBinding]) -> IO MuResult
compileMu dflags metadata stg = return $ stgToMu dflags metadata stg
