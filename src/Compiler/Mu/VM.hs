--
-- Copyright 2017 The Australian National University
--
-- Licensed under the 3-Clause BSD License (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     https://opensource.org/licenses/BSD-3-Clause
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--

module Compiler.Mu.VM (populate) where

import qualified Foreign as F
import qualified Data.ByteString as B
import qualified Foreign.C as C

import Control.Monad.IO.Class (MonadIO, liftIO)

import Mu.API
import Mu.AST
import Mu.Interface

import Compiler.Mu.CodeGen
    
    
-- | Populate a Mu Closure, or other top-level data type, at build-time.
-- Assumes that all the objects in a bundle will have been imported already.

-- XXX: The closures and info tables have fixed size, so we write to the global
-- cell directly. The strings are hybrids though, so the global cell just
-- contains a reference. This reference gets written directly to the closures
-- though, so this only slows down allocation (not access).
populate :: MonadIO m => F.Ptr MuCtx -> TopLevel -> m ()
populate ctx top = case top of
    InfoTableCell n table -> do
        -- Note: this function implcitly asserts that its input is a SimpleInfoTable
        -- Note: 'tableIref' refers to a **simple info table**, i.e. 'infoTable'
        let fillBasicFields :: MonadIO m => InfoTable -> MuIRefValue -> m ()
            fillBasicFields (SimpleInfoTable entry closType) tableIref = do
                closureEntryRef <- getFieldIref ctx tableIref (infoIx EntryField)
                entryRef <- handleFromFunc ctx =<< getID ctx entry
                store ctx muOrdNotAtomic closureEntryRef entryRef

                closureTypeRef <- getFieldIref ctx tableIref (infoIx TypeField)
                closType' <- handleFromSint64 ctx (typeIx closType) 64
                store ctx muOrdNotAtomic closureTypeRef closType'


        let genTable :: MonadIO m => InfoTable -> TypedefName -> m MuIRefValue
            genTable simple tableType = do
                tableIref  <- handleFromGlobal ctx =<< getID ctx n
                
                simpleTableIref <- if (tableType == infoTable) then
                    return tableIref else getFieldIref ctx tableIref 0
                fillBasicFields simple simpleTableIref
                
                return tableIref


        case table of
            SimpleInfoTable {} -> do
                genTable table infoTable
                return ()
            ConsInfoTable simple tag desc -> do
                tableIref  <- genTable simple consInfoTable
                tagRef     <- getFieldIref ctx tableIref (infoIx' ConsTagField)
                tagVal     <- handleFromSint64 ctx (fromIntegral tag) 32
                store ctx muOrdNotAtomic tagRef tagVal
                descDstRef <- getFieldIref ctx tableIref (infoIx' ConsDescField)
                descSrcRef <- handleFromGlobal ctx =<< getID ctx desc
                descVal    <- ctxLoad ctx muOrdNotAtomic descSrcRef
                store ctx muOrdNotAtomic descDstRef descVal
            FunInfoTable simple funType arity -> do
                tableIref <- genTable simple funInfoTable
                typeRef  <- getFieldIref ctx tableIref (infoIx' FunTypeField)
                arityRef <- getFieldIref ctx tableIref (infoIx' FunArityField)
                typeVal  <- handleFromSint64 ctx (fromIntegral funType) 32
                arityVal <- handleFromSint64 ctx (fromIntegral arity) 32
                store ctx muOrdNotAtomic typeRef typeVal
                store ctx muOrdNotAtomic arityRef arityVal
            ThunkInfoTable simple -> do
                tableIref <- genTable simple funInfoTable
                return ()


    Closure n closMuType table payload -> do
        closureIref  <- handleFromGlobal ctx =<< getID ctx n

        -- populate the closure
        header <- getFieldIref ctx closureIref 0
        infoTableField <- getFieldIref ctx header (fieldIx ClosInfoTable)
        tableRef <- handleFromGlobal ctx =<< getID ctx table
        store ctx muOrdNotAtomic infoTableField tableRef
        
        let storePayloads :: MonadIO m => [UnboxedData] -> Int -> m ()  
            storePayloads datums index = case datums of
                []   -> return ()
                d:ds -> do
                    ref <- getFieldIref ctx closureIref $ fromIntegral index
                    case d of
                        UnboxedInt i -> do
                            val <- handleFromSint64 ctx (fromIntegral i) 64
                            store ctx muOrdNotAtomic ref val
                        UnboxedDouble r -> do
                            val <- handleFromDouble ctx (fromRational r)
                            store ctx muOrdNotAtomic ref val
                        TaggedPointer cell tag -> do
                            -- TODO: Incorporate tag once low-bit tagging is implemented
                            tag' <- handleFromSint64 ctx (fromIntegral tag) 3
                            cellID <- getID ctx cell
                            cellRef <- handleFromGlobal ctx cellID
                            store ctx muOrdNotAtomic ref cellRef
                        NullPointer -> do
                            --tag' <- handleFromSint64 ctx 0 6
                            reftyID <- getID ctx muClosureIref
                            nullRef <- handleFromPtr ctx reftyID F.nullPtr
                            store ctx muOrdNotAtomic ref nullRef
                            
                    storePayloads ds (index + 1)
        
        storePayloads payload 1 -- we must start from at least 1, since 0 is the header
                                -- (N.B. This means thunks must explicitly have their
                                -- first fv be null.)


    ByteArray n bs -> do
        -- allocate the byte array
        arrLength <- handleFromSint64 ctx (fromIntegral $ B.length bs) 64
        arrType   <- getID ctx muString
        arrRef    <- newHybrid ctx arrType arrLength

        -- populate the array
        ptr <- pin ctx arrRef >>= handleToPtr ctx 
        liftIO $ B.useAsCStringLen bs $ \(strptr, strlen) -> do
            F.copyBytes (F.castPtr ptr :: C.CString) strptr strlen
        unpin ctx arrRef

        -- attach it to the global cell
        cell <- handleFromGlobal ctx =<< getID ctx n
        store ctx muOrdNotAtomic cell arrRef
    
