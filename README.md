# GHC Mu Backend


## What is this?
As the name suggests, this project aims to compile Haskell programs such that
they can run on the Mu micro virtual machine. It does this by extending GHC's
existing compiler infrastructure. Specifically, we hook into the compilation
process right before the `codeGen` phase, where GHC ordinarily compiles the
`STG` representation of the source code into the `C--` language. The project
is primarily responsible for translating the STG language into efficient Mu IR.

Since the `C--` language operates at a similar level of abstraction to Mu,
the code we write should resemble the `codeGen` code already in GHC. However,
there will be crucial differences. For example, heap allocation and GC are
handled by the VM, so the generated Mu IR will be using the `NEW` and
`NEWHYBRID` instructions instead of explicitly incrementing a heap pointer
and checking if GC needs to be performed as is normally done in GHC's
generated `C--`.

For those unfamiliar with the project, the essential references are the
[Mu specification](https://gitlab.anu.edu.au/mu/mu-spec) and the GHC
[compiler commentary](https://ghc.haskell.org/trac/ghc/wiki/Commentary/Compiler/GeneratedCode).
The latter makes extensive reference to the
[original STG paper](http://research.microsoft.com/en-us/um/people/simonpj/Papers/spineless-tagless-gmachine.ps.gz)
(which used a push-enter execution model), the [eval/apply](https://www.microsoft.com/en-us/research/wp-content/uploads/2016/07/eval-apply.pdf) paper and the
[pointer tagging](http://research.microsoft.com/en-us/um/people/simonpj/papers/ptr-tag/ptr-tagging.pdf)
paper. I also highly recommend watching Simon Peyton Jones's talk on
[Core Haskell](https://www.youtube.com/watch?v=uR_VzYxvbxg)
\- it covers many concepts that translate down into STG as well.


## Where is the code?
The main repository for this project is named `anuhc` in `cabal`'s namespace
and `mu-client-ghc` on the GitLab server. It is primarily responsible for
translating STG semantics into Mu IR. This repository also contains monads
to assist code generation (`Mu` and `Block`) which are specialized for
dealing with Haskell.

The `anuhc` code depends on two supporting libraries (written by us) to
build the IR and load it into the VM. The first is named `mu-pure` in `cabal`'s
namespace and `mu-client-ghc-ast` on GitLab. It defines a Haskell ADT that
represents IR bundles, along with miscellanious facilities like
pretty-printing. Ideally, the AST would allow for Mu IR to be written in
Haskell almost as easily as writing it in text form.

The second is named `mu` in `cabal`'s namespace and `mu-client-ghc-api` in
GitLab's. Its purpose is to expose the Mu API via Haskell FFI calls, and to
provide a layer of abstraction over these that allows, for example, bundles
written in `mu-pure`'s ADT to be loaded into the VM. It is worth mentioning
that both of these repositories use and extend Kunshan's `muapiparser` script
to automatically generate code for the parts of the API that change frequently.
In the case of `mu`, this generates `API.hs` which simply translates all
Mu API functions into Haskell foreign calls (and constants/enumerations into
the appropriate Haskell types). For `mu-pure`, this is mainly used to
enumerate all the common instruction (or "intrinsics") opcodes.


## What needs to be done?
> TODO: update the GitLab issues to reflect this..

The supporting libraries (`mu` and `mu-pure`) are functionally complete. They
will allow full access to the Mu API and work with both the Holstein
(`mu-impl-ref2`) and Zebu (`mu-impl-fast`) Mu implementations, and allow the
loading of arbitrary IR bundles. Thus, the priority is to finish implementing
the translation of STG semantics into Mu IR. Specifically:

- Implement STG semantics in `FromSTG.hs` - look for a `case` matching on
  `StgApp`, `StgLet` etc.

- Extend closure layouts to include [type-specific fields](https://ghc.haskell.org/trac/ghc/browser/includes/rts/storage/InfoTables.h)
  in their info tables. This is difficult because all closures must have
  the same (Mu) type, but different closure types have differing numbers
  of fields. One way to solve this may be to add a pointer to a hybrid
  for "extra fields", though this adds another layer of indirection for
  operations as simple as e.g. querying a function's arity.

- Some of the existing code is using the push-enter execution model - we
  need to change this to eval/apply. (If you look at the eval/apply paper
  each `StgXXX` constructor corresponds to a LHS in figure 2
  "evaluation rules" - if it's not in "rules common to both push/enter
  and eval/apply", it's likely wrong.)
  
- Fix free variable passing - we need to determing which variables need
  to be captured and write them to the payload of the closures, and
  tell our compiler to map Haskell variables (`GHC.Id`) it sees into
  Mu variables (`VarName`). (e.g. `StgLet` needs to bind the Mu name of
  the closure it just allocated so that the inner expression can use it.)
  Have a look at `withFVBindings` to see the general way this is done.
  
- Implement primitive operations. To support (efficient) arithmetic
  operations, I/O, fast applications etc, we will need to write some
  hand-crafted Mu IR that replicates part of the GHC Runtime System. This
  should be as simple as finding the relevant `.cmm` files int eh GHC
  sources and translating them to Mu IR.

Some higher-level questions that we will need to address: (now with answers!)

- How are we doing argument passing? This is easier with eval/apply because
  in principle we will always be passing the correct number of arguments
  to a function. However, we cannot simply make a closure's code take
  arguments through Mu's calling convention, since that will reflect in the
  type of the function reference stored in the closure. This violates
  uniform closure representation, which we need because we do not
  statically know the arity (much less the required arguments) of every
  closure. An inefficient solution would be to pass all arguments in a
  `HYBRID` type. I plan to suggest some kind of varargs-like calling
  convention to be added to the spec, but so far the spec has been fairly
  explicit in avoiding doing this.
  
  > All function references will have type `ref<void> -> ref<void>` until
    we need to call them, at which point we coerce them into the right
    signature.
  
- How will the stack be represented? Mu abstracts over this by giving
  users the `ALLOCA{HYBRID}` primitives to allocate memory on the stack,
  which get freed whever the enclosing function issues `RETURN` or
  `TAILCALL`. This is incompatible with Haskell's approach of keeping
  state of the stack and jumping between functions - we would need a
  lower-level primitive that lets the user push and pop from the stack at
  will. We could do this by having a heap-allocated linked-list, though
  we would be throwing away some of Mu's built-in support for concurrency.
  
  > Both the data and control parts of the stack will live in the heap.
    We will probably "push" by `NEW`ing the cell with a link to the
    previously allocated top cell. (i.e. the stack shall be a heap-allocated
    linked list. Although it is also possible to allocate a massive block 
    of memory and use it as a stack in the sameway GHC does.)
    Concurrency should not be an issue since we can just
    keep multiple pointers to "base frames" and "top frames".
  
- Do we need to optimize the IR? GHC does its optimizations in two phases
  (not counting any of the backends like `llc`); first over the Core
  representation, and second on the generated Cmm. (i.e. the STG->Cmm
  conversion produces inefficient code because it is only the first "pass.")
  It is likely that this is the correct approach for us too, in which case
  we will need to write an IR optimizer, something that's likely going to
  be applicable outside of this project.
  
  > The VM should be able to handle simple cases. We will revisit this when
    we are at a point where can perform benchmarks.
  
- Is there a better way to represent primitives than TagRef64? See
  discussion at [mu/mu-spec#14].
  
  > This is now its own separate issue at [mu/mu-client-ghc#4]. The short
    version is that closures will now be structs of fixed size. This is
    possible if we assume that whether a given free variable is a pointer
    or not never changes, which seems to be the guarantee indirection thunks
    make.
      
Miscellanious tasks:

- Fix modules - at present the code for these is included in the repository,
  referenced via absolute path and compiled regardless of usage. Also
  there's a bug where packages belonging to modules will have a `.` in
  their name which is illegal in Mu.
  
- Make having the libraries for both implementations optional, e.g. create
  a compile-time setting to not link against Holstein. Similarly, allow
  the user to choose which implementation is used to compile the program
  via a command-line flag.
  
- Properly handle GHC flags - need to check if the flag is intended for
  our code or if it should be passed on to GHC.
  
- Figure out how to tell GHC that our `Int`s are 52 bits - see TagRef64
  point above. (And see if there is any other information that should be
  communicated to GHC.)
  
- Add more tests. `mu-pure` and `anuhc` currently have none. `mu` has 5,
  which have already found bugs (eb864d83). However, we need much better
  code coverage (mainly on `Interface.hs`) before we can say with
  confidence that the output is correct. (Neither one of the two Mu
  implementations produce very good error messages, so it can be hard to
  tell if a bug is in the compiler or in the supporting library.)
  
- Make code work on older Haskell versions. (The moma machines needed to
  be upgraded from GHC 7 to 8.)


## Who do I yell at?
This project is under active development, at present by Pavel Zakopaylo
(<u6055204@anu.edu.au>), to whom you should direct most (code-related)
questions.
Previous work was done by Nathan Yong (<nathan.yong@anu.edu.au>) and
Andrew M. Hall (<andrew.hall@anu.edu.au>) - they may be in a better position to
answer certain questions about some of the existing code, but bear in mind that
they are not currently working on this project and thus generally have other
things to do with their time.

Adminstrative inquiries should be sent to the supervisors - namely Steve
Blackburn (<steve.blackburn@anu.edu.au>) and Tony Hosking
(<antony.hosking@anu.edu.au>).

