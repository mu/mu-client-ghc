--
-- Copyright 2017 The Australian National University
--
-- Licensed under the 3-Clause BSD License (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     https://opensource.org/licenses/BSD-3-Clause
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--

{-# LANGUAGE OverloadedStrings #-}

module Compiler.Mu.PrimOps
  (   loadPrim
    , idMuType
    , printIt
  ) where

import Compiler.Mu.CodeGen
import Mu.AST

import TyCon (PrimRep(..), PrimElemRep(..))

import qualified GHC

import DynFlags

-- import RepType (typePrimRep1)
import Type (typePrimRep)

import Data.ByteString.Char8 (pack)
import Lens.Micro.Platform ((%=))
import Data.Monoid ((<>))
import Data.Sequence ((|>))


-- | Determines Mu type of Haskell variable
idMuType :: GHC.Id -> Mu TypedefName
idMuType = primRepMuType . typePrimRep . GHC.idType
    

primRepMuType :: PrimRep -> Mu TypedefName
primRepMuType rep = case rep of
    VoidRep          -> error "panic: primRepMuType:VoidRep"
    --LiftedRep        -> error "not implemented" -- gcWord dflags
    --UnliftedRep      -> error "not implemented" -- gcWord dflags
    PtrRep           -> return muClosureIref
    IntRep           -> return i64 -- bWord dflags
    -- XXX: Assuming Word is 64-bits...
    WordRep          -> return i64 -- bWord dflags
    Int64Rep         -> return i64
    Word64Rep        -> return i64
    AddrRep          -> return muClosureIref -- bWord dflags
    FloatRep         -> return float
    DoubleRep        -> return double
    VecRep len rep'   -> do
        uq <- nextMuUnique
        let n = TypedefName $ Name $ "@vector_type" ++ uq
        elemType <- primElemRepMuType rep'
        typedef n $ Vector elemType len
        return elemType


primElemRepMuType :: PrimElemRep -> Mu TypedefName
primElemRepMuType rep = case rep of
    Int8ElemRep   -> return i8
    Int16ElemRep  -> return i16
    Int32ElemRep  -> return i32
    Int64ElemRep  -> return i64
    Word8ElemRep  -> return i8
    Word16ElemRep -> return i16
    Word32ElemRep -> return i32
    Word64ElemRep -> return i64
    FloatElemRep  -> return float
    DoubleElemRep -> return double


-- | Emits mu IR to print a static string
printIt :: BasicBlockName -> String -> ExpWriter ()
printIt n s = do
    uq <- liftExpr $ nextMuUnique
    let name' = VarName $ Name $ "@_message" <> uq
    let name = GlobalCellName $ Name $ "@_message" <> uq
    
    liftExpr $ definitions %= (|> GlobalCell name muStringRef)
    liftExpr $ topLevels %= (|> ByteArray name (pack s))
    
    strLen <- upConstant $ length s
    
    strRef     <- assign n $ Load False Nothing muStringRef name' Nothing
    strPtr     <- assign n $ Comminst CiUvmNativePin [] [muStringRef] [] [strRef] Nothing Nothing
    strVoidPtr <- assign n $ ConvertOperation PTRCAST muStringPtr ptr_void strPtr
    _          <- assign n $ CCall MuCallConvention writePtrType writeSig writePtr' [stderr', strVoidPtr, strLen] Nothing Nothing
    emit $ Comminst CiUvmNativeUnpin [] [muStringRef] [] [strRef] Nothing Nothing
    
  where
  
    writePtr' = constToVar writePtr
    stderr'   = constToVar stderr


loadPrim :: DynFlags -> MuResult
loadPrim dflags = bundleMu dflags "__prim__ops__" $ do
    funcsig  writeSig [i32, ptr_void, i64] [i64]
    typedef  writePtrType $ UFuncPtr writeSig
    constant writePtr writePtrType $ ExternCtor $ pack "write"

    stub "@_not_implemented"
    dconStub "@zdghczmprimzdGHCziTypeszdModule"
    dconStub "@zdghczmprimzdGHCziTypeszdTyCon"
    dconStub "@zdghczmprimzdGHCziTypeszdTrNameS"
    dconStub "@zdghczmprimzdGHCziTypeszdTrNameD"


stub :: FunctionName -> Mu ()
stub n = primfuncdef n (placeholderBlocks n)


dconStub :: Name -> Mu ()
dconStub (Name n) = do
    uid <- nextMuUnique
    let cName = GlobalCellName $ Name $ n <> uid <> "_str"
    
    definitions %= (|> GlobalCell cName muStringRef)
    topLevels %= (|> ByteArray cName (pack n))
    
    stub funcName

    emitInfo (GlobalCellName $ Name $ n <> "_static_info") $
        ConsInfoTable (SimpleInfoTable funcName StaticConstructor) 0 cName
        
    emitInfo (GlobalCellName $ Name $ n <> "_con_info") $
        ConsInfoTable (SimpleInfoTable funcName Constructor) 0 cName
    
    -- XXX: We're unlikely to need wrappers for these, but you never know..

  where

    funcName = FunctionName $ Name $ n <> "_static_entry"


placeholderBlocks :: FunctionName -> Blocks BasicBlock
placeholderBlocks n = 
    basicBlock bbName [(param, muClosureIref)] $ do
        printIt bbName "FATAL: Not implemented\n"
        return $ Comminst CiUvmThreadExit [] [] [] [] Nothing Nothing
  where
    bbName = n `dot` "entry"
    param = bbName `dot` "param"
