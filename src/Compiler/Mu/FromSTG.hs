--
-- Copyright 2017 The Australian National University
--
-- Licensed under the 3-Clause BSD License (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     https://opensource.org/licenses/BSD-3-Clause
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--

{-# LANGUAGE OverloadedStrings #-}

module Compiler.Mu.FromSTG
  (
  -- * Main entry points
  stgToMu
  ) where


import Data.Map.Strict (Map)
import Control.Monad (void, when, foldM)
import Control.Monad.IO.Class (MonadIO, liftIO)
import Control.Monad.Reader (Reader, ReaderT, MonadReader, runReaderT, ask, local)
import Control.Monad.State.Strict (State, StateT, MonadState, evalState, runStateT, gets, modify)
import Control.Monad.Trans (MonadTrans, lift)
import Control.Monad.Writer (Writer, WriterT, MonadWriter, runWriterT, tell)
import Data.Char (ord)
import Data.String (fromString)
import Data.Monoid ((<>))
import qualified Data.Map.Strict as M
import Data.ByteString.Char8 (pack)

import Lens.Micro.Platform ((%=))
import Data.Sequence (Seq, (|>))

import Literal (Literal(..))
import DataCon (DataCon, dataConWrapId, dataConRepRepArity, dataConTag, isUnboxedTupleCon, dataConName)
import BasicTypes (RecFlag(..), TopLevelFlag(..))
import Outputable (Outputable, pprTraceIt)
import Language.Haskell.GHC.Simple
import GHC (ModSummary(..), Id)
import DynFlags (DynFlags)
import TyCon (TyCon, isEnumerationTyCon, tyConDataCons)
import StgSyn

import qualified GHC as GHC
import qualified Literal as Lit

import Compiler.Mu.PrimOps (idMuType, printIt)

import qualified Var as Var

import Compiler.Mu.CodeGen
import Mu.AST

import Debug.Trace

import StgCmmClosure
import StgCmmEnv
import StgCmmLayout
import StgCmmMonad (cg_lf)
import Type


-- | Compile the information inside an STG module into a 'Bundle'.
stgToMu :: DynFlags -> ModMetadata -> ([TyCon], [StgBinding]) -> MuResult
stgToMu dflags modData (tyCons, bindings) =
  bundleMu dflags modName $ do
    loadTypes
    mapM_ codegenTop bindings
    traceM "Finished translating bindings"
    mapM_ codegenType tyCons
  where
    modName = traceShowId $ mmName modData


-- | Generate code for a top-level STG binding in Mu.
codegenTop :: StgBinding -> Mu ()
codegenTop binding = do
    dflags <- gets _dflags
    case binding of
        StgNonRec bindId rhs -> do
            let (info, code) = codegenTop' dflags bindId rhs
            code
            topBinds %= (uncurry M.insert info)
            
        StgRec pairs -> do
            let (binds, codes) = unzip $ map (uncurry $ codegenTop' dflags) pairs
            mapM_ (\v -> topBinds %= (uncurry M.insert v)) binds
            sequence_ codes


-- | Generate code for a type.
codegenType :: TyCon -> Mu ()
codegenType tycon = if isEnumerationTyCon tycon
    then codegenEnumerations tycon
        else mapM_ codegenDataCon (tyConDataCons tycon)


-- | Generate the static reference code for the entries of enumerated data
-- types.
codegenEnumerations :: TyCon -> Mu ()
codegenEnumerations _tycon = mapM_ codegenDataCon $ tyConDataCons _tycon


-- | Generate the static closures for data constructor fields.
codegenDataCon :: DataCon -> Mu ()
codegenDataCon dcon = do
    traceShowM ("Inside DataCon: ", dataConEntry dcon)
    -- store the name of this constructor
    cName <- string (dataConName dcon) (pack $ tail $ stringify $ dataConName dcon)
    
    -- When a DataCon is entered, tag the pointer (if possible) and
    -- jump to call continuation
    funcdef funcName (Version "1") muClosureFunctionSig $
        basicBlock entry [(this, muClosureIref)] $ do
            tag <- upTag (dataConTag dcon)
            -- TODO: Tag 'this' with 'tag' once low-bit tagging is implemented
            
            return $ Return [this]

    -- Generate info table for all constructors of this DataCon type
    emitInfo (staticInfoNameOf dcon) $
        ConsInfoTable (SimpleInfoTable funcName StaticConstructor) (dataConTag dcon) cName
        
    emitInfo (infoNameOf dcon) $
        ConsInfoTable (SimpleInfoTable funcName Constructor) (dataConTag dcon) cName
    
    -- TODO: wrappers 

      where
        funcName = dataConEntry dcon
        entry = funcName `dot` "entry"
        this = entry `dot` "this"
        


-------------------------------------------------- Utilities


codegenTop' :: DynFlags -> GHC.Id -> StgRhs -> ((GHC.Id, (GlobalCellName, LambdaFormInfo)), Mu ())
codegenTop' dflags name rhs = traceShow ("Compiling ", name) $ case rhs of
    StgRhsCon _cc con args -> ((name, (staticInfoNameOf name, mkConLFInfo con)), code)
    
      where
        
        code = do
            argLits <- mapM literal args
            closure name (staticInfoNameOf con) argLits

    StgRhsClosure _cc _bindinfo nonglobfrees updateFlag _srt args body ->
        ((name, (closureNameOf name, mkClosureLFInfo dflags name TopLevel [] updateFlag args)), code)
        
          where
          
            code = do
                mod <- gets _moduleName
                when (tail (stringify name) == ("zdmainzd" <> mod <> "zdmain")) $ do
                    traceM "Found main method!"
                    modify (\v -> v { _mainFunc = Just (primordialEntry, closureNameOf' name,
                        closureRefTypeNameOf name) })
            
                -- No free variables b/c we are at top level (i.e. not within a let binding)
                when (not $ null nonglobfrees) $
                    error "panic: top-level closures should only have top-level free variables"

                (entry, _) <- closureBody name args [] body
                emitInfo (conInfoNameOf name) (infoTable' args entry)
                closure name (conInfoNameOf name) (payload' args)

                  where

                    -- XXX: funType unused
                    infoTable' args entry = case args of
                        [] -> ThunkInfoTable (SimpleInfoTable entry StaticThunk)
                        _  -> FunInfoTable (SimpleInfoTable entry StaticFunction) 0 (length args)
                    
                    payload' args = case args of
                        [] -> [NullPointer] -- thunk, needs spot for indirection
                        l  -> []            -- function

                    primordialEntry = case rhs of
                        StgRhsCon _ con _ -> dataConEntry' con
                        StgRhsClosure  {} -> entryNameOf'  name


-- | Taken from compiler/codeGen/StgCmmBind.hs in GHC
-- since it's not publicly exported

-- XXX: The impl of this changes after 8.0.2
mkClosureLFInfo :: DynFlags
                -> Id           -- The binder
                -> TopLevelFlag -- True of top level
                -> [NonVoid Id] -- Free vars
                -> UpdateFlag   -- Update flag
                -> [Id]         -- Args
                -> LambdaFormInfo
mkClosureLFInfo dflags bndr top fvs upd_flag args
  | null args =
        mkLFThunk (GHC.idType bndr) top (map unsafe_stripNV fvs) upd_flag
  | otherwise =
        mkLFReEntrant top (map unsafe_stripNV fvs) args (mkArgDescr dflags args)


-- | Convert STG literal into primitive type
literal :: StgArg -> Mu UnboxedData
literal arg = case arg of
    StgLitArg lit -> case lit of
        Lit.MachInt    i   -> return $ UnboxedInt    i
        Lit.MachInt64  i   -> return $ UnboxedInt    i
        Lit.MachWord   i   -> return $ UnboxedInt    i
        Lit.MachWord64 i   -> return $ UnboxedInt    i
        Lit.MachFloat  r   -> return $ UnboxedDouble r
        Lit.MachDouble r   -> return $ UnboxedDouble r
        Lit.LitInteger i _ -> return $ UnboxedInt    i
        Lit.MachChar   c   -> return $ UnboxedInt $ fromIntegral (ord c)
        Lit.MachStr    str -> do
            uid <- nextMuUnique
            let strName = GlobalCellName $ Name ("@_str" ++ uid)
            string' strName str
            return $ TaggedPointer strName 0
        Lit.MachNullAddr -> do
            return NullPointer
        Lit.MachLabel {} -> do
            -- TODO: replace with something else
            return NullPointer
    StgVarArg argId -> return $ TaggedPointer (closureNameOf argId) 0


-- | Generate the closure code corresponding to an 'StgExpr'.
closureBody :: GHC.Id -> [GHC.Id] -> [GHC.Id] -> StgExpr -> Mu (FunctionName, TypedefName)
closureBody name _ _ _ | traceShow ("closurebody", (entryNameOf name)) False = undefined
closureBody name args fvars body = do
    args' <- mapM idMuType args
    funcsig funcSig (muClosureIref : args') [muClosureIref]
    typedef funcTy $ FuncRef funcSig
    
    let muNames    = map (dot entry . tail . stringify) args
    let namedArgs  = zip muNames args'
    let namedArgs' = (this, muClosureIref) : namedArgs
    let boundArgs  = zip3 muNames args' (map mkLFArgument args)
    let boundArgs' = zip args boundArgs
    
    funcdef funcName (Version "1") funcSig $ do
        bindings <- getLocalBinds
        basicBlock entry namedArgs' $ do
            let argBind = foldl (\e -> \(n, v) -> M.insert n v e) bindings boundArgs'
        
            (withFVBindings entry this fvars (null args) $ do
                sequel <- lift $ expression name body
                traceShowM ("gonna branch to ", sequel)
                boundNames <- sortedIDNames
                return $ Branch1 $ DestinationClause sequel boundNames) `capturing` argBind
            
    return (funcName, funcTy)
    
  where
  
    entry = funcName `dot` "entry"
    this = funcName `dot` "entry.this"
    funcName = entryNameOf name
    funcSig = signatureNameOf funcName
    funcTy = funTypeNameOf funcName


-- | Bind some free variables from a closure to actual Mu values.
--
-- For this to work, there need to be at least as many values in the closure as
-- there are arguments that need to be bound.

withFVBindings :: BasicBlockName       -- ^ Name of the current 'BasicBlock'
             -> VarName                -- ^ Variable pointing to the closure with the
                                       --   arguments to bind
             -> [GHC.Id]               -- ^ Variables to bind
             -> Bool                   -- ^ Is it a thunk?
             -> ExpWriter a
             -> ExpWriter a
withFVBindings name this fvars isThunk code = do
    (_, muClosRefType) <- liftExpr $ genClosType (Left fvars) isThunk
    thisIref <- assign name $ ConvertOperation REFCAST muClosureIref muClosRefType this

    let bindVar :: (Int, CapturedIDs) -> GHC.Id -> ExpWriter (Int, CapturedIDs)
        bindVar (index, cIDs) var = do
            var' <- liftExpr $ idMuType var
            ref <- assign name $ GetFieldIRef False var' index thisIref
            val <- assign name $ Load False Nothing var' thisIref Nothing
            (_, _, lf_info) <- lookupID var -- supposedly we already know its binding
            return (index + 1, M.insert var (val, var', lf_info) cIDs)

    bindings   <- lift $ getLocalBinds
    -- If this is thunk, field 1 is a null pointer so start at 2
    -- (index 0 is closure header)
    (_, cIDs') <- foldM bindVar (if isThunk then 2 else 1, bindings) fvars

    code `capturing` cIDs'


-- | Create and populate a closure's info table at runtime
mkInfo :: BasicBlockName    -- ^ Outer basic block, for naming purposes
       -> RunTimeInfoTable  -- ^ Static parameters
       -> ExpWriter (VarName, TypedefName, TypedefName)
mkInfo block table = do
    -- generate table
    infoRef   <- assign block $ New infoMuType Nothing
    infoIref  <- assign block $ GetIRef infoMuType infoRef
    simpleInfoIref <- if infoMuType /= infoTable then
        assign block $ GetFieldIRef False infoMuType 0 infoIref
            else return infoIref
    
    infoType' <- upValue (IntCtor $ fromIntegral $ fromEnum infoType) i32
    
    -- fill basic fields
    entryFieldRef <- assign block $ GetFieldIRef False infoTable (fromEnum EntryField) simpleInfoIref
    emit $ Store False Nothing muClosureFunction entryFieldRef (funToVar entry) Nothing
    typeFieldRef  <- assign block $ GetFieldIRef False infoTable (fromEnum TypeField) simpleInfoIref
    emit $ Store False Nothing i32 typeFieldRef infoType' Nothing
    
    -- fill type-specific fields
    case table of
        RTSimpleInfoTable {} -> return ()
        RTConsInfoTable _ tag desc -> do
            tagRef  <- assign block $ GetFieldIRef False infoMuType (infoIx' ConsTagField) infoIref
            emit $ Store False Nothing i32 tagRef tag Nothing
            descRef <- assign block $ GetFieldIRef False infoMuType (infoIx' ConsDescField) infoIref
            descVal <- assign block $ Load False Nothing muStringRef (fromGlobal desc) Nothing
            emit $ Store False Nothing muStringRef descRef descVal Nothing
        RTFunInfoTable _ funType arity -> do
            typeRef  <- assign block $ GetFieldIRef False infoMuType (infoIx' FunTypeField)  infoIref
            arityRef <- assign block $ GetFieldIRef False infoMuType (infoIx' FunArityField) infoIref
            emit $ Store False Nothing i32 typeRef  funType Nothing
            emit $ Store False Nothing i32 arityRef arity   Nothing
        RTThunkInfoTable _ -> do
            return ()
    
    return (infoIref, infoMuType, infoMuRefType)

  where

    infoType :: ClosureType -- help the type inferencer out a bit..
    (infoMuType, infoMuRefType, entry, infoType) = case table of
        RTSimpleInfoTable e i -> (infoTable, infoTableIref, e, i)
        RTConsInfoTable  (RTSimpleInfoTable e i) _ _ -> (consInfoTable, consInfoTableIref, e, i)
        RTFunInfoTable   (RTSimpleInfoTable e i) _ _ -> (funInfoTable, funInfoTableIref, e, i)
        RTThunkInfoTable (RTSimpleInfoTable e i)     -> (thunkInfoTable, thunkInfoTableIref, e, i)
        
    funToVar :: FunctionName -> VarName
    funToVar (FunctionName n) = VarName n
    
    fromGlobal :: GlobalCellName -> VarName
    fromGlobal (GlobalCellName n) = VarName n


-- | Figures out the Mu type of a closure
genClosType :: Either [GHC.Id] [StgArg] -> Bool -> Mu (TypedefName, TypedefName)
genClosType fvars isThunk = do
    uq <- nextMuUnique
    let closTypeName = TypedefName $ Name $ "@_local_clos_defn" <> uq
    let closRefTypeName = TypedefName $ Name $ "@_local_ref_clos_defn" <> uq
    
    let start = if isThunk then [muClosure, muClosureIref] else [muClosure]
    fvars' <- (case fvars of
        Left  val -> mapM idMuType val
        Right val -> mapM (\v -> literal v >>= return . unboxedType) val)
    
    typedef closTypeName (Struct $ start ++ fvars')
    typedef closRefTypeName (IRef closTypeName)
    
    return (closTypeName, closRefTypeName)


-- | Create a Mu Closure at runtime
mkClosure :: BasicBlockName           -- ^ Outer basic block, for naming purposes
          -> VarName                  -- ^ Reference to info table
          -> Either [GHC.Id] [StgArg] -- ^ List of free variables
          -> Bool                     -- ^ Is it a thunk?
          -> ExpWriter (VarName, TypedefName, TypedefName)
mkClosure block table fvars isThunk = do
    (closTypeName, closTypeRefName) <- liftExpr $ genClosType fvars isThunk

    -- create the closure
    clos  <- assign block $ New closTypeName Nothing
    clos' <- assign block $ GetIRef closTypeName clos
    
    -- write info table to closure
    headerRef   <- assign block $ GetFieldIRef False closTypeName 0 clos'
    infoRefRef <- assign block $ GetFieldIRef False muClosure (fromEnum ClosInfoTable) headerRef
    emit $ Store False Nothing infoTableIref infoRefRef table Nothing
    
    return (clos', closTypeName, closTypeRefName)
    
  
-- | Populate a Mu closure at runtime  
populateClosurePayload :: BasicBlockName
                       -> (VarName, TypedefName, TypedefName) -- output from mkClosure
                       -> Either [GHC.Id] [StgArg]
                       -> Bool
                       -> ExpWriter () 
populateClosurePayload block (clos, closTypeName, closTypeRefName) fvars isThunk = do
    let fvars' :: [Either GHC.Id StgArg]
        fvars' = case fvars of
            Left  vars -> map Left  vars
            Right vars -> map Right vars
        
    let storePayloads :: [Either GHC.Id StgArg] -> Int -> ExpWriter ()
    	storePayloads datums index = case datums of
    	    []   -> return ()
    	    d:ds -> do
                index' <- upConstant index
                ref <- assign block $ GetFieldIRef False closTypeName index clos
                (sto, dty) <- (case d of
                    Left val -> do
                        (sto', dty', _) <- lookupID val
                        return (sto', dty')
                    
                    Right val -> case val of
                        StgVarArg var -> do
                            (sto', dty', _) <- lookupID var
                            return (sto', dty')
                    
                        StgLitArg _ -> do
                            un <- liftExpr $ literal val
                            let dty' = unboxedType un
                            sto' <- (case un of
                                UnboxedInt i         -> upValue (IntCtor i) dty'
                                UnboxedDouble r      -> upValue (DoubleCtor $ fromRational r) dty'
                                TaggedPointer gcn _i -> return $ toVarName gcn -- FIXME: incorporate tag once it is implemented
                                NullPointer          -> upValue NullCtor dty')
                            
                            return (sto', dty'))
                
                emit $ Store False Nothing dty ref sto Nothing
                
                storePayloads ds (index + 1)
    			
    storePayloads fvars' (if isThunk then 2 else 1)
    
  where
   
    fst3 :: (a, b, c) -> a
    fst3 (x, _, _) = x


instance Show Var.Var where
    show = show . entryNameOf


expression :: GHC.Id                 -- ^ The ID of the enclosing closure this belongs to
           -> StgExpr                -- ^ STG Expression to translate
           -> Blocks BasicBlockName  -- ^ The name of the block that starts
                                     --   this expression
expression name topExpr = do
    bindings <- getLocalBinds
    dflags <- liftBlocks $ gets _dflags
    traceShowM ("inside expression for: ", entryNameOf name)
    -- traceShowM ("bindings are: ", bindings)
    case topExpr of

        StgApp fun_id args -> do
        
            (fun, fun_type, lf_info) <- lookupIDBlocks fun_id
            
            case isInternal fun_id of
                True  -> traceShowM ("internal StgApp", entryNameOf fun_id)
                False -> traceShowM ("external StgApp", entryNameOf fun_id)
            
            -- XXX: If we need to the the whole externalize thing, we will
            -- need to add the ID to the CapturedIDs tuple
            let cg_fun_id   = fun_id

                fun_arg     = StgVarArg cg_fun_id
                fun_name    = idName    cg_fun_id
                n_args      = length args
                v_args      = length $ filter (isVoidTy . stgArgType) args
                node_points dflags = nodeMustPointToIt dflags lf_info
            
            -- TODO: SelfLoopInfo, i.e. efficient tail calls
            case getCallMethod dflags fun_name cg_fun_id lf_info n_args v_args
                (LneLoc undefined undefined) -- prevent error when getCallMethod returns JumpToIt
                    Nothing {-self_loop_info-} of
                        ReturnIt               -> traceM "ReturnIt"    >> muEmitReturn entry [fun]
                        EnterIt                -> traceM "EnterIt"     >> muEmitEnter  entry (fun, fun_type)
                        SlowCall               -> traceM "SlowCall"    >> placeholderBlocks entry
                        DirectEntry lbl arity  -> traceM "DirectEntry" >> placeholderBlocks entry
                        JumpToIt _ _           -> traceM "JumpToIt"    >> placeholderBlocks entry

        StgConApp con args
            | isUnboxedTupleCon con -> traceShow ("unboxed StgConApp", dataConEntry con)
                -- $ mkClosure (entryNameOf name) (dataConEntry con) args (\clos -> Return [clos])
                $ error "StgConApp not implemented"
            -- error . show $ ("StgConApp unboxedtuple not implemented", closureNameOf name)
            | otherwise -> traceShow ("StgConApp", dataConEntry con)
            	-- $ mkClosure (entryNameOf name) (dataConEntry con) args (\clos -> Return [clos])
            	$ error "StgConApp not implemented"

        StgOpApp {} -> error "StgOpApp not implemented"

        StgCase {} -> error "StgCase not implemented"

        StgLet binds expr -> do
            traceShowM ("StgLet", showBind binds)
            uq <- liftBlocks nextMuUnique
            boundNames <- sortedParams
            oldBinds <- getLocalBinds
            
            let allocBlockName = entry `dot` ("alloc" ++ uq)
            bb <- basicBlock allocBlockName [] $ case binds of
                StgNonRec bindId rhs -> do
                    (bind, code) <- codegenLetBind allocBlockName bindId rhs
                    code
                    (do
                        exprBlock <- lift $ expression name expr
                        boundNames' <- sortedIDNames
                        return $ Branch1 $ DestinationClause exprBlock boundNames')
                            `capturing` (uncurry M.insert bind $ oldBinds)
                            
                StgRec pairs -> do
                    (binds, codes) <- return . unzip =<< mapM (uncurry $ codegenLetBind allocBlockName) pairs
                    let newBinds = foldl (\old -> \(bindId, bind) -> M.insert bindId bind old) oldBinds binds
                    (do
                        sequence_ codes
                        exprBlock <- lift $ expression name expr
                        boundNames' <- sortedIDNames
                        return $ Branch1 $ DestinationClause exprBlock boundNames')
                            `capturing` newBinds
                            
            emitBB bb
            return allocBlockName

          where
          
            showBind (StgNonRec bindId _) = show $ closureNameOf bindId
            showBind (StgRec binds) = show $ fmap (closureNameOf . fst) binds
            

        -- FIXME: As is obvious, this is just doing the same thing as StgLet.
        --        This kills tail-call performance.
        StgLetNoEscape _live_lhs _live_rhs binds expr -> expression name (StgLet binds expr)

        StgLam {} -> error "panic: found a StgLam expression"
        StgLit {} -> error "StgLit not implemented"
        StgTick {} -> error "StgTick not implemented"

      where

        entry = entryNameOf name
        i64_0' = constToVar i64_0


-- | Emit: (i) pointer to allocated (but not populated) closure,
--         (ii) code to populate the closure
--
-- Note that the info table (and most importantly, entry code)
-- is populated straight away because the entry code loads the
-- fvs from the closure itself.
codegenLetBind :: BasicBlockName
               -> GHC.Id
               -> StgRhs
               -> ExpWriter ((GHC.Id, (VarName, TypedefName, LambdaFormInfo)), ExpWriter ())
codegenLetBind block bindId rhs = case rhs of
    StgRhsClosure _cc _bindinfo nonglobfrees updateFlag _srt args body -> do
        dflags <- liftExpr $ gets _dflags
        let lf_info = mkClosureLFInfo dflags bindId NotTopLevel (nonVoidIds nonglobfrees) updateFlag args
        (allocClosureBody'', funTy) <- liftExpr $ closureBody bindId args nonglobfrees body
        allocClosureBody' <- assign block $ ConvertOperation REFCAST funTy muClosureFunction $ toVarName allocClosureBody''
        let allocClosureBody = toFunName allocClosureBody'
        
        info'' <- (case args of
            [] -> do
                return $ RTThunkInfoTable (RTSimpleInfoTable allocClosureBody Thunk)
            -- XXX: funType not used
            _  -> do
                arity <- upValue (IntCtor $ fromIntegral $ length args) i32
                funType <- upValue (IntCtor 0) i32
                return $ RTFunInfoTable (RTSimpleInfoTable allocClosureBody Function) funType arity)
        (info', _, infoRefTy) <- mkInfo block info''
        info <- assign block $ ConvertOperation REFCAST infoRefTy infoTableIref info'
        
        closDef@(clos, closType, closRefType) <- mkClosure block info (Left nonglobfrees) (null args)
        clos' <- assign block $ ConvertOperation REFCAST closRefType muClosureIref clos
        
        return ((bindId, (clos', muClosureIref, lf_info)), populateClosurePayload block closDef (Left nonglobfrees) (null args))
        
    StgRhsCon _cc con args -> do
        let lf_info = mkConLFInfo con
        info <- assign block $ ConvertOperation REFCAST consInfoTableIref infoTableIref (toVarName $ staticInfoNameOf con)
        closDef@(clos, closType, closRefType) <- mkClosure block (toVarName $ staticInfoNameOf con) (Right args) False
        clos' <- assign block $ ConvertOperation REFCAST closRefType muClosureIref clos
        
        return ((bindId, (clos', muClosureIref, lf_info)), populateClosurePayload block closDef (Right args) False)
        
  where
    
    toFunName :: VarName -> FunctionName
    toFunName (VarName n) = FunctionName n


-- FIXME: 'muEmitReturn' and 'muEmitEnter' need to send the bound params to the new
--         basic block (for sequel \in MuBranchTo) along with the return values

-- | See 'emitReturn' in StgCmmLayout
muEmitReturn :: FunctionName -> [VarName] -> Blocks BasicBlockName
muEmitReturn entry vals = do
    sequel <- getSequel
    uq <- liftBlocks nextMuUnique
    boundNames <- sortedParams
    
    let bbName = entry `dot` ("returnBlock" ++ uq)
    bb <- basicBlock bbName [] $ case sequel of
        MuReturn -> return $ Return vals
        MuBranchTo targetBB -> return $ Branch1 $ DestinationClause targetBB vals
        
    emitBB bb
    return bbName


-- | See 'emitEnter' in StgCmmExpr
muEmitEnter :: FunctionName -> (VarName, TypedefName) -> Blocks BasicBlockName
muEmitEnter entry (val, muType) = do
    sequel <- getSequel
    uq <- liftBlocks nextMuUnique
    boundNames <- sortedParams
    
    let bbName = entry `dot` ("entryBlock" ++ uq)
    bb <- basicBlock bbName [] $ do
        -- TODO: move this gorp to another function?
        headerRef  <- assign bbName $ ConvertOperation REFCAST muType muClosure val
        infoRefRef <- assign bbName $ GetFieldIRef False muClosure (fromEnum ClosInfoTable) headerRef
        infoRef    <- assign bbName $ Load False Nothing infoTableIref infoRefRef Nothing
        entryRef   <- assign bbName $ GetFieldIRef False infoTable (infoIx EntryField) infoRef
        entry      <- assign bbName $ Load False Nothing muClosureFunction entryRef Nothing
        
        case sequel of
            MuReturn            -> return $ TailCall muClosureFunctionSig entry [headerRef]
            MuBranchTo targetBB -> do
                -- FIXME: We do not handle the possibility of the thunk having multiple returns
                --        (When does this happen, again?)
                callResult <- assign bbName $ Call muClosureFunctionSig entry [headerRef]
                    Nothing Nothing
                
                return $ Branch1 $ DestinationClause targetBB [callResult]
    emitBB bb
    return bbName
            

-- Note [Applications]
-- ~~~~~~~~~~~~~~~~~~~
--
-- StgApp bindings can be used for many different things; for example the `id`
-- function translates to an StgApp of the first argument (whatever it is).
-- This might be a function, it might be a data value, it could be anything.


placeholderBlocks :: FunctionName -> Blocks BasicBlockName
placeholderBlocks n = do
    bb <- basicBlock bbName [] $
        return $ Comminst CiUvmThreadExit [] [] [] [] Nothing Nothing
    emitBB bb
    return bbName
  where
    bbName = n `dot` "placeholder"
    param = bbName `dot` "param"
    retVal = bbName `dot` "ret"


_pprError
    :: Outputable a
    => String -> a -> b
_pprError msg val = seq (pprTraceIt msg val) undefined


